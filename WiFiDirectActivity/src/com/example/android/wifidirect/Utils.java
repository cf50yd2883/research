package com.example.android.wifidirect;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.util.Scanner;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import android.util.Log;

public class Utils {

	private final static String p2pInt = "p2p-p2p0";

	public static String getIPFromMac(String MAC) {
		/*
		 * method modified from:
		 * 
		 * http://www.flattermann.net/2011/02/android-howto-find-the-hardware-mac-address-of-a-remote-host/
		 * 
		 * */
        String result = null;
		BufferedReader br = null;
        Scanner sk = null;
		try {
			br = new BufferedReader(new FileReader("/proc/net/arp"));
            sk = new Scanner(new File("/proc/net/arp"));
			String line;
			while ((line = br.readLine()) != null) {
           // while((line = sk.nextLine()) != null){
                //line = sk.nextLine();
				String[] splitted = line.split(" +");
				if (splitted != null && splitted.length >= 4) {
					// Basic sanity check
                    for(int i = 0; i < splitted.length; i++){
                        Log.d(WiFiDirectActivity.TAG, "i is " + i + " MAC IS " + splitted[i] + " mac is " + MAC);
                    
					String device = splitted[5];
					if (device.matches(".*" +p2pInt+ ".*")){
						String mac = splitted[3];
						if (mac.equals(splitted[i])) {//used to be matches not equals
					//		return splitted[0];
                            result = splitted[0];
                            Log.d(WiFiDirectActivity.TAG, "Splitted IS " + splitted[0]);
						}else{
                            Log.d(WiFiDirectActivity.TAG, "COULDING FIND MAC");
                        }
					}else{
                        Log.d(WiFiDirectActivity.TAG, "COULDN'T SPLIT PROPERLY");
                    }
                    }

				}else{
                    Log.d(WiFiDirectActivity.TAG, "SPLITTED IS NULL");
                }
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//return null;
        Log.d(WiFiDirectActivity.TAG, "ADDRESS IS " + result);
        return result;
	}


	public static String getLocalIPAddress() {
		/*
		 * modified from:
		 * 
		 * http://thinkandroid.wordpress.com/2010/03/27/incorporating-socket-programming-into-your-applications/
		 * 
		 * */
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();

					String iface = intf.getName();
					if(iface.matches(".*" +p2pInt+ ".*")){
						if (inetAddress instanceof Inet4Address) { // fix for Galaxy Nexus. IPv4 is easy to use :-)
							return getDottedDecimalIP(inetAddress.getAddress());
						}
					}
				}
			}
		} catch (SocketException ex) {
			Log.e("AndroidNetworkAddressFactory", "getLocalIPAddress()", ex);
		} catch (NullPointerException ex) {
			Log.e("AndroidNetworkAddressFactory", "getLocalIPAddress()", ex);
		}
		return null;
	}

	private static String getDottedDecimalIP(byte[] ipAddr) {
		/*
		 * ripped from:
		 * 
		 * http://stackoverflow.com/questions/10053385/how-to-get-each-devices-ip-address-in-wifi-direct-scenario
		 * 
		 * */
		String ipAddrStr = "";
		for (int i=0; i<ipAddr.length; i++) {
			if (i > 0) {
				ipAddrStr += ".";
			}
			ipAddrStr += ipAddr[i]&0xFF;
		}
		return ipAddrStr;
	}
}
