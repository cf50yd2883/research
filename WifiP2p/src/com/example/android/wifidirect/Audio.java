package com.example.android.wifidirect;
import android.media.AudioTrack;
import android.media.AudioFormat;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.AudioManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import android.os.Environment;
import android.content.DialogInterface;
import android.util.Log;

//added by me
import android.os.ParcelFileDescriptor;
import java.io.FileDescriptor;
import android.media.AudioRecord;
import android.content.res.AssetManager;
import android.media.audiofx.NoiseSuppressor;
import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.AudioEffect;
import android.content.Context;
import android.media.AudioTrack;
import android.media.AudioManager;
import android.media.AudioFormat;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import android.os.ParcelFileDescriptor.AutoCloseInputStream;
import android.os.ParcelFileDescriptor.AutoCloseOutputStream;
import java.util.Timer;
import java.util.TimerTask;

public class Audio{
    //private static MediaPlayer mPlayer = null;
    //private static MediaRecorder mRecorder = null;
    //for streaming
    private static final int RECORDER_BPP = 16;
	private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
	private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
	private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
	private static final int RECORDER_SAMPLERATE = 44100;//was 44100
	//private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_STEREO;;
	private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_CONFIGURATION_MONO;;
	private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
   // private static String filepath = null;	
	private static AudioRecord recorder = null;
    private static AudioTrack at = null;
	private static int bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, 
            RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
	private static Thread recordingThread = null;
	private static boolean isRecording = false;
    public static int off = 0;
    public static int id = 0;
    //for listening
    public static class WaveInfo{
        public int rate;
        public int channels;
        public int dataSize;
        public WaveInfo(int rate, int channels, int dataSize){
            this.rate = rate;
            this.channels = channels;
            this.dataSize = dataSize;
        }
    }
    public static void Listen(InputStream is){
        //WaveInfo info = readHeader(is);
        WaveInfo wave = readHeader(is);
        if(wave!=null){
            play(readWavPcm(wave, is));
        }
        //Log.d(WiFiDirectActivity.TAG, "LISTENING");
    }
    public static void startPlay(){
        at = new AudioTrack(AudioManager.STREAM_VOICE_CALL, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, bufferSize, AudioTrack.MODE_STREAM, 0);
        at.play();
    }
    public static void stopPlay(){
        at.stop();
        at.release();
    }
    public static void play(byte[] byteData){
        if(at!=null){
            at.write(byteData, 0, byteData.length);
        }else{
            Log.d(WiFiDirectActivity.TAG, "COULDN'T INITIALIZE AUDIO TRACK");
        }
    }
    public static byte[] readWavPcm(WaveInfo info, InputStream stream){
        byte[] data=new byte[info.dataSize];
        //Log.d(WiFiDirectActivity.TAG, "READING HEADER");
        try{
            stream.read(data, 0, data.length);
            stream.close();
        }catch(IOException e){
            Log.d(WiFiDirectActivity.TAG, "COULDN'T READ FILE");
        }
        return data;
    }
    public static WaveInfo readHeader(InputStream wavStream){
        //Log.d(WiFiDirectActivity.TAG, "READ HEADER");
        ByteBuffer buffer = ByteBuffer.allocate(44);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        
        try{
            wavStream.read(buffer.array(), buffer.arrayOffset(), buffer.capacity());
        }catch(IOException e){
            Log.d(WiFiDirectActivity.TAG, "COULDN'T READ HEADER STREAM");
        }
        buffer.rewind();
        buffer.position(buffer.position() + 20);
        //Log.d(WiFiDirectActivity.TAG, "BUFFER POSITION IS " + buffer.position());
        //check format
        int format = buffer.getShort();
        checkFormat(format == 1, "Unsupported encoding: " + format);
        //check channels
        int channels = buffer.getShort();
        checkFormat(channels ==1 || channels == 2, "Unsupported channels: " + channels);
        //check sample rate
        int rate = buffer.getInt();
        checkFormat(rate <= 48000 && rate >= 11025, "Unsupported rate: " + rate);
        //back to business
        buffer.position(buffer.position()+6);
        int bits = buffer.getShort();
        checkFormat(bits==16, "Unsupported bits: " + bits);
        int dataSize = 0;
        while(buffer.getInt() != 0x61746164){
            Log.d(WiFiDirectActivity.TAG, "Skipping nonData Chunk");
            int size = buffer.getInt();
            
            try{
                wavStream.skip(size);
            }catch(Exception e){
                Log.d(WiFiDirectActivity.TAG, "COULDN'T SKIPP");
            }
            buffer.rewind();
            try{
                wavStream.read(buffer.array(), buffer.arrayOffset(), 8);
            }catch(IOException e){
                Log.d(WiFiDirectActivity.TAG, "Couldn't read the Stream");
            }
            buffer.rewind();
        }
       //Log.d(WiFiDirectActivity.TAG, "BUFFER POSTIIONT IS " + buffer.position());
       dataSize = buffer.getInt();
       checkFormat(dataSize > 0, "wrong datasize: " + dataSize);
       return new WaveInfo(rate, channels, dataSize);
    }
    public static void checkFormat(boolean Success, String message){
        if(!Success){
            Log.d(WiFiDirectActivity.TAG, message);
            //System.exit(0);
        }
    }
	/*	
	private static String getFilename(){
		//filepath = Environment.getExternalStorageDirectory().getPath();
		File file = new File(filepath,AUDIO_RECORDER_FOLDER);
		
		if(!file.exists()){
			file.mkdirs();
		}
		
		return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_WAV);
	}
	
	private static String getTempFilename(){
		//String filePath = Environment.getExternalStorageDirectory().getPath();
		File file = new File(filepath,AUDIO_RECORDER_FOLDER);
		
		if(!file.exists()){
			file.mkdirs();
		}
		
		//File tempFile = new File(filepath,AUDIO_RECORDER_TEMP_FILE);
        File tempFile = new File(filepath,AUDIO_RECORDER_TEMP_FILE);
		
		if(tempFile.exists())
			tempFile.delete();
		
		return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
	}
	*/
    //for fTP	
    /*
	public static void Talking(String FilePath){
        filepath = FilePath;
		recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
						RECORDER_SAMPLERATE, RECORDER_CHANNELS,RECORDER_AUDIO_ENCODING, bufferSize);
		
		recorder.startRecording();
		
		isRecording = true;
		
		recordingThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				writeAudioDataToFile();
			}
		},"AudioRecorder Thread");
		
		recordingThread.start();
	}
    private static void writeAudioDataToFile(){
		byte data[] = new byte[bufferSize];
		String filename =filepath+".raw"; 
		//String filename = getTempFilename();
		FileOutputStream os = null;
		
		try {
			os = new FileOutputStream(filename);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int read = 0;
		
		if(null != os){
			while(isRecording){
				read = recorder.read(data, 0, bufferSize);
				
				if(AudioRecord.ERROR_INVALID_OPERATION != read){
					try {
						os.write(data);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void stopTalking(){
		if(null != recorder){
			isRecording = false;
			
			recorder.stop();
			recorder.release();
			
			recorder = null;
			recordingThread = null;
		}
		
		//copyWaveFile(getTempFilename(),getFilename());
        copyWaveFile(filepath+".raw",filepath+".wav");
		deleteTempFile();
	}

	private static void deleteTempFile() {
		//File file = new File(getTempFilename());
		File file = new File(filepath+".raw");
		
		file.delete();
	}
	
	private static void copyWaveFile(String inFilename,String outFilename){
        Log.d(WiFiDirectActivity.TAG, "TEMP PATH IS " + inFilename + " REAL PATH IS "+outFilename);
		FileInputStream in = null;
		FileOutputStream out = null;
		long totalAudioLen = 0;
		long totalDataLen = totalAudioLen + 36;
		long longSampleRate = RECORDER_SAMPLERATE;
		int channels = 2;
		long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels/8;
		
		byte[] data = new byte[bufferSize];
                
		try {
			in = new FileInputStream(inFilename);
			out = new FileOutputStream(outFilename);
			totalAudioLen = in.getChannel().size();
			totalDataLen = totalAudioLen + 36;
			
		    Log.d(WiFiDirectActivity.TAG, "SIZE IS " + totalAudioLen);	
			WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
					longSampleRate, channels, byteRate);
			
			while(in.read(data) != -1){
				out.write(data);
			}
			
			in.close();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    private static void WriteWaveFileHeader(
			FileOutputStream out, long totalAudioLen,
			long totalDataLen, long longSampleRate, int channels,
			long byteRate) throws IOException {
		
		byte[] header = new byte[44];
	    //set totalAudioLen to 8192 for stereo
        //set totalAudioLen to 4096 for mono
		header[0] = 'R';  // RIFF/WAVE header
		header[1] = 'I';
		header[2] = 'F';
		header[3] = 'F';
		header[4] = (byte) (totalDataLen & 0xff);
		header[5] = (byte) ((totalDataLen >> 8) & 0xff);
		header[6] = (byte) ((totalDataLen >> 16) & 0xff);
		header[7] = (byte) ((totalDataLen >> 24) & 0xff);
		header[8] = 'W';
		header[9] = 'A';
		header[10] = 'V';
		header[11] = 'E';
		header[12] = 'f';  // 'fmt ' chunk
		header[13] = 'm';
		header[14] = 't';
		header[15] = ' ';
		header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
		header[17] = 0;
		header[18] = 0;
		header[19] = 0;
		header[20] = 1;  // format = 1
		header[21] = 0;
		header[22] = (byte) channels;
		header[23] = 0;
		header[24] = (byte) (longSampleRate & 0xff);
		header[25] = (byte) ((longSampleRate >> 8) & 0xff);
		header[26] = (byte) ((longSampleRate >> 16) & 0xff);
		header[27] = (byte) ((longSampleRate >> 24) & 0xff);
		header[28] = (byte) (byteRate & 0xff);
		header[29] = (byte) ((byteRate >> 8) & 0xff);
		header[30] = (byte) ((byteRate >> 16) & 0xff);
		header[31] = (byte) ((byteRate >> 24) & 0xff);
		header[32] = (byte) (2 * 16 / 8);  // block align
		header[33] = 0;
		header[34] = RECORDER_BPP;  // bits per sample
		header[35] = 0;
		header[36] = 'd';
		header[37] = 'a';
		header[38] = 't';
		header[39] = 'a';
		header[40] = (byte) (totalAudioLen & 0xff);
		header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
		header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
		header[43] = (byte) ((totalAudioLen >> 24) & 0xff);
        out.write(header, 0, 44);    
	}
    */
    //for Streaming
    public static void Talking(ParcelFileDescriptor[] Pipes, final String mac, final int port){
        off = 0;
		recorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT,
						RECORDER_SAMPLERATE, RECORDER_CHANNELS,RECORDER_AUDIO_ENCODING, bufferSize);
		recorder.startRecording();
        Log.d(WiFiDirectActivity.TAG, "AUDIO SESSION ID IS " + recorder.getAudioSessionId());
		isRecording = true;
	    final FileDescriptor outputPipe = Pipes[1].getFileDescriptor();	
        final ParcelFileDescriptor pipe = Pipes[1];
        //new Timer().schedule(new TimerTask(){
            recordingThread = new Thread(new Runnable() {
                
                @Override
                public void run() {
                    //writeAudioDataToFile();
                    writeAudioToSocket(outputPipe, pipe, mac, port);
                }
            },"AudioRecorder Thread");
            recordingThread.start();
        //}, 300);
	}
    private static void writeAudioToSocket(FileDescriptor SocketPipe, ParcelFileDescriptor pipe, final String mac, final int port){
		byte data[] = new byte[bufferSize];
        AutoCloseOutputStream os = null;
        //surgery
        long totalAudioLen = 0;
		long totalDataLen = totalAudioLen + 36;
		long longSampleRate = RECORDER_SAMPLERATE;
		int channels = RECORDER_CHANNELS;
		long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels/8;
	    int i = 0;	

		try {
            os = new AutoCloseOutputStream(pipe);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int read = 0;
	    //for checking
        id = 0;
		if(null != os){
			while(isRecording){
                Log.d(WiFiDirectActivity.TAG, "HAVE SEND " + id + "TIMERS");
				read = recorder.read(data, 0, bufferSize);
                if(AudioRecord.ERROR_INVALID_OPERATION != read){
                    try {
                        //surgery
                        if(RECORDER_CHANNELS == AudioFormat.CHANNEL_IN_STEREO){
                            totalAudioLen = 8192;
                        }else{
                            totalAudioLen = 4096;
                        }
                        byte payload[] = WriteWaveFileHeader(totalAudioLen, totalDataLen,
                            longSampleRate, channels, byteRate, data);
                        //Listen(new ByteArrayInputStream(payload));
                        Sox.UDPsend(mac, port, payload, totalAudioLen, null);
                        //want to try
                        os.write(payload);
                        os.flush();
                        //works
                        //os.write(data);
                        //os.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
			}
			
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public static void PrepWaveFile(final String IP, final int port, ParcelFileDescriptor pipe){
		AutoCloseInputStream in = null;
		/*FileInputStream in = null;
		long totalAudioLen = 0;
		long totalDataLen = totalAudioLen + 36;
		long longSampleRate = RECORDER_SAMPLERATE;
		int channels = 2;
		long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels/8;
	    //want to try	
        */
        long totalAudioLen = 0;
        long totalDataLen = 0;
		byte[] data = new byte[44+bufferSize];
		try {
            in = new AutoCloseInputStream(pipe);
            if(in != null){
                int len = 0;
                while((len = in.read(data))!=-1){
                    if(RECORDER_CHANNELS == AudioFormat.CHANNEL_IN_STEREO){
                        totalAudioLen = 8192;
                    }else{
                        totalAudioLen = 4096;
                    }
                    totalDataLen = totalAudioLen + 36;
                    //Listen(new ByteArrayInputStream(payload));
                    Log.d(WiFiDirectActivity.TAG, "MAC IS " + IP);
                    Sox.UDPsend(IP, port, data, totalAudioLen, null);
                }
                in.close();
            }else{
                Log.d(WiFiDirectActivity.TAG, "CANNOT CREATE PIPE");
                isRecording = false;
            }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    public static void stopTalking(){
		if(null != recorder){
			isRecording = false;
			
			recorder.stop();
			recorder.release();
			
			recorder = null;
			recordingThread = null;
		}
		
	}
	private static byte[] WriteWaveFileHeader(
			long totalAudioLen,
			long totalDataLen, long longSampleRate, int channels,
			long byteRate, byte[] data) throws IOException {
		byte[] header = new byte[44];
        byte[] payload = new byte[data.length+44];
	    //set totalAudioLen to 8192 for stereo
        //set totalAudioLen to 4096 for mono
		header[0] = 'R';  // RIFF/WAVE header
		header[1] = 'I';
		header[2] = 'F';
		header[3] = 'F';
		header[4] = (byte) (totalDataLen & 0xff);
		header[5] = (byte) ((totalDataLen >> 8) & 0xff);
		header[6] = (byte) ((totalDataLen >> 16) & 0xff);
		header[7] = (byte) ((totalDataLen >> 24) & 0xff);
		header[8] = 'W';
		header[9] = 'A';
		header[10] = 'V';
		header[11] = 'E';
		header[12] = 'f';  // 'fmt ' chunk
		header[13] = 'm';
		header[14] = 't';
		header[15] = ' ';
		header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
		header[17] = 0;
		header[18] = 0;
		header[19] = 0;
		header[20] = 1;  // format = 1
		header[21] = 0;
		header[22] = (byte) channels;
		header[23] = 0;
		header[24] = (byte) (longSampleRate & 0xff);
		header[25] = (byte) ((longSampleRate >> 8) & 0xff);
		header[26] = (byte) ((longSampleRate >> 16) & 0xff);
		header[27] = (byte) ((longSampleRate >> 24) & 0xff);
		header[28] = (byte) (byteRate & 0xff);
		header[29] = (byte) ((byteRate >> 8) & 0xff);
		header[30] = (byte) ((byteRate >> 16) & 0xff);
		header[31] = (byte) ((byteRate >> 24) & 0xff);
		header[32] = (byte) (2 * 16 / 8);  // block align
		header[33] = 0;
		header[34] = RECORDER_BPP;  // bits per sample
		header[35] = 0;
		header[36] = 'd';
		header[37] = 'a';
		header[38] = 't';
		header[39] = 'a';
		header[40] = (byte) (totalAudioLen & 0xff);
		header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
		header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
		header[43] = (byte) ((totalAudioLen >> 24) & 0xff);
        Log.d(WiFiDirectActivity.TAG, "ENDING IS " + header[40] + header[41] + header[42] + header[43] );
	//	out.write(header, 0, 44);
        System.arraycopy(header, 0, payload, 0, header.length);
        System.arraycopy(data, 0, payload, header.length, data.length);
        return payload;
	}
//end stream
/*
    public static void startRecording(String FilePath){
        //Log.d(WiFiDirectActivity.TAG, "Starting to record Audio");
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(FilePath);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try{
            mRecorder.prepare();
        }catch(IOException e){
            //Log.d(WiFiDirectActivity.TAG, "Failed to create recorder");
        }
        mRecorder.start();
    }
    public static void stopRecording(){
        //Log.d(WiFiDirectActivity.TAG, "STOPPING RECORDING");
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
    }

    public byte[] getData(String File){
        byte buf[] = new byte[1024];
        int len;
        try{
            FileInputStream fis = new FileInputStream(new File(File));
            while((len = fis.read(buf))!=-1){
               return buf; 
            }
        }catch(Exception e){
               //Log.d(WiFiDirectActivity.TAG, "READING FILE FAILED");
               return buf;
        }
        return buf;
    }
    
    public static void startPlay(File f){
        mPlayer = new MediaPlayer();
        try{
           String path = f.toString();
           mPlayer.setDataSource(path);
           mPlayer.prepare();
           mPlayer.start();
        }catch(Exception e){
            //Log.d(WiFiDirectActivity.TAG, "FAILED TO PLAY AUDIO " +e.getMessage());
        }
    }
    public static void stopPlay(){
        mPlayer.release();
        mPlayer = null;
    }
    */
}
