/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.wifidirect;

import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Collection;
import java.util.TimerTask;
import java.util.Timer;
/**
 * A ListFragment that displays available peers on discovery and requests the
 * parent activity to handle user interaction events
 */
public class DeviceListFragment extends ListFragment implements PeerListListener {

    private List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    private List<WifiP2pDevice> things = new ArrayList<WifiP2pDevice>();
    ProgressDialog progressDialog = null;
    View mContentView = null;
    private WifiP2pDevice device;
    private boolean invite = false;
    private boolean running = false;
    public static WifiP2pConfig config = null;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setListAdapter(new WiFiPeerListAdapter(getActivity(), R.layout.row_devices, peers));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContentView = inflater.inflate(R.layout.device_list, null);
        return mContentView;
    }

    /**
     * @return this device
     */
    public WifiP2pDevice getDevice() {
        return device;
    }

    private static String getDeviceStatus(int deviceStatus) {
        Log.d(WiFiDirectActivity.TAG, "Peer status :" + deviceStatus);
        switch (deviceStatus) {
            case WifiP2pDevice.AVAILABLE:
                return "Available";
            case WifiP2pDevice.INVITED:
                return "Invited";
            case WifiP2pDevice.CONNECTED:
                return "Connected";
            case WifiP2pDevice.FAILED:
                return "Failed";
            case WifiP2pDevice.UNAVAILABLE:
                return "Unavailable";
            default:
                return "Unknown";

        }
    }

    /**
     * Initiate a connection with the peer.
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        WifiP2pDevice device = (WifiP2pDevice) getListAdapter().getItem(position);
        ((DeviceActionListener) getActivity()).showDetails(device);
    }

    /**
     * Array adapter for ListFragment that maintains WifiP2pDevice list.
     */
    private class WiFiPeerListAdapter extends ArrayAdapter<WifiP2pDevice> {

        private List<WifiP2pDevice> items;

        /**
         * @param context
         * @param textViewResourceId
         * @param objects
         */
        public WiFiPeerListAdapter(Context context, int textViewResourceId,
                List<WifiP2pDevice> objects) {
            super(context, textViewResourceId, objects);
            items = objects;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_devices, null);
            }
            WifiP2pDevice device = items.get(position);
            Log.d(WiFiDirectActivity.TAG, "DEVICE NAME IS " + device.deviceName);
            if (device != null) {
                TextView top = (TextView) v.findViewById(R.id.device_name);
                TextView bottom = (TextView) v.findViewById(R.id.device_details);
                if (top != null) {
                    top.setText(device.deviceName);
                }
                if (bottom != null) {
                    bottom.setText(getDeviceStatus(device.status));
                }
            }

            return v;

        }
    }

    /**
     * Update UI for this device.
     * 
     * @param device WifiP2pDevice object
     */
    public void updateThisDevice(WifiP2pDevice device) {
        this.device = device;
        TextView view = (TextView) mContentView.findViewById(R.id.my_name);
        view.setText(device.deviceName);
        view = (TextView) mContentView.findViewById(R.id.my_status);
        view.setText(getDeviceStatus(device.status));
    }
    public void setList(ArrayList<WifiP2pDevice> network){
        while(true){
            peers.addAll(Sox.ListServer(50000));
            ((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();
        }
    }
    public void setPeers(final WifiP2pGroup group){
        Collection<WifiP2pDevice> Neighbors = group.getClientList(); 
        peers.clear();
        peers.add(BroadcastSetup());
        ((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();
        peers.addAll(Neighbors);
        ((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();
        Thread t = new Thread(new Runnable(){
            public void run(){
                Sox.distribute(peers, 50000);
            }
        });
        try{
            t.start();
            t.join();
        }catch(Exception e){
            Log.d(WiFiDirectActivity.TAG, "Sending network information failed " + e.getMessage());
        }
    }
    //added
    private List<ScanResult> APScan(){
        running = false;
        List<ScanResult> APs = new ArrayList<ScanResult>(); 
        List<WifiConfiguration> point = new ArrayList<WifiConfiguration>(); 
        WifiManager mana = (WifiManager)getActivity().getSystemService(getActivity().WIFI_SERVICE);
        long start = System.nanoTime();
        if(mana.startScan()){
            Log.d(WiFiDirectActivity.TAG, "SCAN SUCCESSFUL");
            APs = mana.getScanResults();
            Log.d(WiFiDirectActivity.TAG, "SCANNED FOR " + (System.nanoTime() - start));
        }
        if(mana.pingSupplicant()){
            point = mana.getConfiguredNetworks();
        }
        try{
            super.finalize();
        }catch(Throwable e){
            Log.d(WiFiDirectActivity.TAG, "FAILED TO CLEAR AP SCANNER");
        }
        if(point.isEmpty()){
            Log.d(WiFiDirectActivity.TAG, "NO CONFIGURED NETWORKS TT_TT TT_TT TT_TT");
        }else{

        }
        mana = null;
        return findDirect(APs);
    }
    private List<ScanResult>findDirect(List<ScanResult> APs){
        List<ScanResult> Aps = new ArrayList<ScanResult>();
        /*for(int k = 0; k < APs.size(); k++){
            Log.d(WiFiDirectActivity.TAG, "LIST OF POINTS "+APs.get(k).toString());
        }*/
        String[] SSID=null;
        String Direct = "DIRECT";
        int j = 0;
        for(int i = 0; i < APs.size(); i++){
            SSID = APs.get(i).SSID.split("-");          
            if(SSID[0].equals(Direct)){
                Aps.add(j++, APs.get(i));
            }
        }
        APs.clear();
        APs = null;
        return Aps;
    }
    private void join(ScanResult AP){
        Log.d(WiFiDirectActivity.TAG, "TRYING TO CONNECT" + Utils.converter(AP.BSSID));
        ProgressDialog progressDialog = null;


        //here need to make more memory efficient

        //WifiP2pConfig config = new WifiP2pConfig();
        config = new WifiP2pConfig();
        config.deviceAddress = Utils.converter(AP.BSSID);
        config.wps.setup = WpsInfo.PBC;
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
//        progressDialog = ProgressDialog.show(getActivity(), "Press back to cancel",
//                "Connecting to :" + config.deviceAddress, true, true
//        );
        ((DeviceActionListener) getActivity()).connect(config);
    }
    private WifiP2pDevice BroadcastSetup(){
        WifiP2pDevice Broadcast = new WifiP2pDevice();
        //set Address to 00:00:00:00:00:00
        Broadcast.deviceAddress = Sox.Broad;
        //set Device name
        Broadcast.deviceName = "Broadcast";
        //setprimary device type
        Broadcast.primaryDeviceType = "1-0050F204-1"; 
        //set secondarDeivceType
        Broadcast.secondaryDeviceType = null;
        //set status to available;
        Broadcast.status = 0;
        return Broadcast; 
    }
    //added end
    @Override
    public void onPeersAvailable(WifiP2pDeviceList peerList) {
        //Log.d(WiFiDirectActivity.TAG, "FOUND PEERS");
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        //should never delete peers want to maintain a list(for time being will make automatic later)
        //peers.clear();
        //Log.d(WiFiDiectActivity.TAG, "THIS IS MY LIST OF PEERS " + peerList.toString());
        if(peers.isEmpty()){ 
            peers.add(BroadcastSetup());
            ((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();
        }
        things.addAll(peerList.getDeviceList());
        for(int i = 0; i < things.size(); i++){
            if(!peers.contains(things.get(i))){
                peers.add(i, things.get(i));
                ((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();
            }
        }
        Intent intent = new Intent();
        intent.setAction("android.intent.action.ALIVE");
        //this.sendBroadcast(intent);
        Log.d(WiFiDirectActivity.TAG, "SENDING BEACON");
        //((DeviceActionListener) getActivity()).Broadcast(intent);
        Random r = new Random();
        final int value = 0000+Math.abs(r.nextInt()%9000);
        r = null;
        Log.d(WiFiDirectActivity.TAG, "Wont SCAN for " + value + " cycles");
        r = null;
        if (peers.size() == 0) {
            //create group here? also remove initiatediscover
            for(int a = 0; a < value; a++){
                int i = 0;
                i = i+a;
            }
            ((DeviceActionListener) getActivity()).Create();
            Log.d(WiFiDirectActivity.TAG, "No devices found");
            return;
        }else{//added from else
            //if(!group.isOwner()){
            //move to on create? ad make it timed?
            Thread scan = new Thread(new Runnable(){
                public void run(){
                    new Timer().schedule(new TimerTask(){
                        public void run(){
                            int z; 
                            List<ScanResult> GO = new ArrayList<ScanResult>();
                            for(z = 0; z < value; z++){
                                GO = APScan();
                            //}
                                if(GO.isEmpty()){
                                    Log.d(WiFiDirectActivity.TAG, "LIST IS EMPTY");
                                    //create
                //                    ((DeviceActionListener) getActivity()).Create();
                                }else{
                                    Log.d(WiFiDirectActivity.TAG, "LIST IS NOT EMPTY NOT PRINTING");
                                    for(int q = 0; q < GO.size(); q++){
                                        Log.d(WiFiDirectActivity.TAG, GO.get(q).toString());
                                    }
                                    join(GO.get(0));
                                    z = value+1;
                                }
                                GO.clear();
                                GO = null;
                            }
                            if(z <= value){
                                ((DeviceActionListener) getActivity()).Create();
                            }
                        }
                    }, 5000);
                }
            });
            try{
                scan.start();
                scan.join();
                scan = null;
            }catch(Exception e){
                Log.d(WiFiDirectActivity.TAG, "COULDN'T Start Scanning");
            }
        }
        things.clear();
    }
    public void reconnect(final WifiP2pConfig config){
        Thread NOA = new Thread(new Runnable(){
            public void run(){
                List<ScanResult> GROUP = new ArrayList<ScanResult>();
                GROUP = APScan();
                for(int i = 0; i < GROUP.size(); i++){
                    if(config.deviceAddress.equals(Utils.converter(GROUP.get(i).BSSID))){
                        Log.d(WiFiDirectActivity.TAG, " RECONNECTING TO " + GROUP.get(i).toString());

                        ((DeviceActionListener) getActivity()).connect(config);
                       // join(GROUP.get(i));
                    }else{
                        Log.d(WiFiDirectActivity.TAG, "COULDN'T Reconnect to group");
                    }
                }
            }
        });
        try{
            Log.d(WiFiDirectActivity.TAG, "trying to return to group");
            NOA.start();
            NOA.join();
        }catch(Exception e){
            Log.d(WiFiDirectActivity.TAG, "Failed to join NOA thread "+e.getMessage());
        }
    }
    public void clearPeers() {
        Log.d(WiFiDirectActivity.TAG, "Clearing list");
        peers.clear();
        ((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();
    }

    /**
     * 
     */
    public void onInitiateDiscovery() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = ProgressDialog.show(getActivity(), "Press back to cancel", "finding peers", true,
                true, new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        
                    }
                });
    }

    /**
     * An interface-callback for the activity to listen to fragment interaction
     * events.
     */
    public interface DeviceActionListener {

        void NoA();

        void NoW();

        void showDetails(WifiP2pDevice device);

        void cancelDisconnect();

        void Create();

        void Broadcast(Intent intent);

        void connect(WifiP2pConfig config);

        void disconnect();
    }

}
