package com.example.android.wifidirect;
import java.net.Socket;
import java.net.ServerSocket;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.net.wifi.p2p.WifiP2pDevice;
import android.util.Log;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import java.io.FileDescriptor;
import java.io.ByteArrayInputStream;
import java.lang.String;
import java.io.FileOutputStream;

import com.example.android.wifidirect.DeviceListFragment.DeviceActionListener;
public class Sox{
    private static boolean running = false;
    private static File f = null;
    public static String Broad = new String("FF:FF:FF:FF:FF:FF");
    //private static String mFile = "/mnt/sdcard/DCIM/rec.pcm";
    private static String mFile = "/mnt/sdcard/DCIM/Audiozz.wav";
    private static String client_mac_fixed = null;
    public static void setMac(String mac){
        client_mac_fixed = mac;
    }
    public static void UDPserver(){
        Audio.startPlay();
        try{
            byte[] recievedData = new byte[4140+Broad.getBytes().length];
            byte[] audioData = new byte[4140];
            DatagramSocket server = new DatagramSocket(12345);
            DatagramPacket received = new DatagramPacket(recievedData, recievedData.length);
            byte[] address = new byte[(int) Broad.getBytes().length];
            byte[] id = new byte[4];
            String IP = null;
            String from = null;
            while(running){
                
                server.receive(received);
                //Log.d(WiFiDirectActivity.TAG, "RECIRVED");
                String destination = null;
                try{
                    System.arraycopy(recievedData, 0, audioData, 0, 4140);
                    System.arraycopy(recievedData, 4140, address, 0, Broad.getBytes().length);
                }catch(IndexOutOfBoundsException e){
                    Log.d(WiFiDirectActivity.TAG, "INDext is out of bounds in udpserver " + e.getMessage());
                }catch(ArrayStoreException e){
                    Log.d(WiFiDirectActivity.TAG, "ArraySTORE EXCEPTION UDP SERVER " + e.getMessage());
                }catch(NullPointerException e){
                    Log.d(WiFiDirectActivity.TAG, "NULL POINTER EXCETPION UDP SERVER " + e.getMessage());
                }
                try{
                    destination = new String(address, "UTF-8");
                }catch(Exception e){
                    Log.d(WiFiDirectActivity.TAG, "FAILED TO CREATE STRING OF DESTINATION MAC");
                }
                IP = Utils.getIPFromMac(destination);
                from = received.getAddress().toString();
                ByteArrayInputStream aus = new ByteArrayInputStream(audioData);
                if(audioData[20] ==1){
                    if(destination.equals(Broad)||IP != null){                
                        IP = null;
                        if(destination.equals(Broad)){
                            Audio.Listen(aus);
                        }
                        UDPsend(destination, 12345, audioData, 4096, from.substring(1));
                    }else{
                        Audio.Listen(aus);
                    }
                }else{
                    Log.d(WiFiDirectActivity.TAG, "BAD ARRAY SKIPPED SERVER");
                }
                destination = null;
                aus.close();
            }
            Audio.stopPlay();
        }catch(Exception e){
            e.printStackTrace();
            Log.d(WiFiDirectActivity.TAG, "ServerSocket couldn't be made");
        }
    }
    public static void UDPsend(String mac, int port, byte[] stuff, long length, String From){
        byte[] buf = new byte[4096+44+client_mac_fixed.getBytes().length];
        DatagramSocket socket = null;
        String IP = null;
        try{
            if(length != buf.length){
                System.arraycopy(stuff, 0, buf, 0, stuff.length);
                stuff = null;
            }
            System.arraycopy(client_mac_fixed.getBytes(), 0, buf, 4140, client_mac_fixed.getBytes().length);
            socket = new DatagramSocket();
            int len = 0;
            DatagramPacket sendPacket = null;
            if(buf[20] == 1){
                if(mac.equals(Broad)){
                    Log.d(WiFiDirectActivity.TAG, "MAC TO SEND TO IS " + mac + " Broadcast is " + Broad);
                    String[] YellowPages = Utils.getAddress();
                    for(int i = 0; i < YellowPages.length; i++){
                        Log.d(WiFiDirectActivity.TAG, "SENDING TO " + YellowPages[i]);
                        if(!YellowPages[i].equals(From)){
                            sendPacket = new DatagramPacket(buf, buf.length, InetAddress.getByName(YellowPages[i]), port); 
                            socket.send(sendPacket);
                        }
                    }
                }else{
                   Log.d(WiFiDirectActivity.TAG, "SENDING TO " + Utils.getIPFromMac(mac));
                   IP = Utils.getIPFromMac(mac);
                   if(IP != null){
                        sendPacket = new DatagramPacket(buf, buf.length, InetAddress.getByName(Utils.getIPFromMac(mac)), port); 
                   }else{
                        sendPacket = new DatagramPacket(buf, buf.length, InetAddress.getByName("192.168.49.1"), port); 
                   }
                   socket.send(sendPacket);
                }
            }else{
                Log.d(WiFiDirectActivity.TAG, "BAD ARRAY SKIPPED SENDER");
            }
        }catch(Exception e){
            Log.d(WiFiDirectActivity.TAG, "FAILED TO CREATE UDP SOCKET " + e.getMessage());
        }
    }

    public static void send(Socket socket, InputStream inputstream){
        int len;
        byte buf[] = new byte[1024];
        try{
            if((socket != null)&&(socket.isConnected())){
                byte[] address = client_mac_fixed.getBytes();
                OutputStream os = socket.getOutputStream();
                os.write(address, 0, address.length);
                os.flush();

                copyFile(inputstream, os);
                socket.close();
                socket = null;
            }
        }catch(Exception e){
            Log.d(WiFiDirectActivity.TAG, "SOME MESSAGE FAILED TO CONNECT");
        }
    }
    //if we do this from WiFiDirectBroadcastRecieverpublic static void distribute(boolean GO, Collection<WifiP2pDevice> neightbors, DeviceListFragment activity){
    public static void distribute(List<WifiP2pDevice> neightbors, int port){
        Socket socket = null;
        ObjectOutputStream OOS = null;
        for(int i = 0; i < neightbors.size(); i++){
            //construct new list
            ArrayList<WifiP2pDevice> network = new ArrayList<WifiP2pDevice>();
            if(neightbors != null){
            try{
                Log.d(WiFiDirectActivity.TAG, "SENDING TO " + Utils.getIPFromMac(neightbors.get(i).deviceAddress));
                socket = new Socket(InetAddress.getByName(Utils.getIPFromMac(neightbors.get(i).deviceAddress)), port);
                OOS = new ObjectOutputStream(socket.getOutputStream());
                OOS.writeObject(network);
                OOS.flush();
                OOS.close();
                socket.close();
                network = null;
            }catch(IOException e){
                Log.d(WiFiDirectActivity.TAG, "DISTRIBUTE FAILED " + e.getMessage());
            }
            }
        }
    }
    public static List<WifiP2pDevice> ListServer(int LOCAL){
        ArrayList<WifiP2pDevice> Directory = new ArrayList<WifiP2pDevice>();
        ServerSocket server = null;
        try{
            server = new ServerSocket(LOCAL);
        }catch(IOException e){
            Log.d(WiFiDirectActivity.TAG, "FAILED TO CREATE LIST SERVER "+e.getMessage());
        }
        //while(running){
            try{
                Socket catcher = server.accept();
                ObjectInputStream OIS = new ObjectInputStream(catcher.getInputStream());
                Directory = (ArrayList<WifiP2pDevice>) OIS.readObject();
         //       List.setList(Directory);
            }catch(Exception e){
                Log.d(WiFiDirectActivity.TAG, "FAILED TO READ FROM LIST: " + e.getMessage());
            }
        //}
        if(!running){
            try{
                server.close();
                server = null;
            }catch(Exception e){
                Log.d(WiFiDirectActivity.TAG, "FAILED TO CLOSE SERVER");
            }
        }
        return Directory;
    }
    public static void startServer(){
        running = true;
    }
    public static void stopServer(){
        running = false;
    }
    public static boolean copyFile(InputStream inputStream, OutputStream out){
        //byte buf[] = new byte[1024];
        int len;
        byte buf[] = new byte[4140];
        //byte buf[] = new byte[8192+44];
        try{
            while((len = inputStream.read(buf)) != -1){
                Log.d(WiFiDirectActivity.TAG, "LEN IN COPY FILE IS " + len);
                out.write(buf, 0, len);
                out.flush();
                Log.d(WiFiDirectActivity.TAG, "COPY FILE TO OUT STREAM");
            }
            out.close();
            inputStream.close();
        }catch(Exception e){
            Log.d(WiFiDirectActivity.TAG, "FAILED COPY " + e.getMessage());
            return false;
        }
        return true;
    }
    public static void server(int port){
        ServerSocket server = null;
        String destination = null;
        Socket client = null;
        try{
            server = new ServerSocket(port);
        }catch(Exception e){
            Log.d(WiFiDirectActivity.TAG, "Failed to create Server "+e.getMessage());
        }
        while(running){
            if(!running){
                try{
                    client.close();
                    server.close();
                }catch(Exception e){
                    Log.d(WiFiDirectActivity.TAG, "Failed to close sockets");
                }
            }
            try{
                client = server.accept();
                f = new File(mFile);
                InputStream inputStream = client.getInputStream();
                byte[] address = new byte[(int) client_mac_fixed.getBytes().length];
                int len = inputStream.read(address);
                destination = new String(address, "UTF-8");
                if(destination.equals(Broad)){
                    //broadcast

                    copyFile(inputStream, new FileOutputStream(f));
                    String[] IP = Utils.getAddress();
                    for(int i = 0; i < IP.length; i++){
                        String IPaddress = IP[i];
                        Log.d(WiFiDirectActivity.TAG, "IP IS " + IPaddress);
                        if(!IPaddress.equals("192.168.49.1")&&!IPaddress.equals(client.getInetAddress().toString().substring(1))){
                            send(new Socket(InetAddress.getByName(IPaddress), port), new FileInputStream(f));
                        }
                    }
                    Audio.Listen(new FileInputStream(f));
                }else{
                    //surgery
                    copyFile(inputStream, new FileOutputStream(f));
                    String clientIP = Utils.getIPFromMac(destination);
                    Log.d(WiFiDirectActivity.TAG, "NEW IP IS " + clientIP);
                    Log.d(WiFiDirectActivity.TAG, "NEW MAC IS " + destination);
                    if(clientIP == null){
                        Audio.Listen(new FileInputStream(f));
                    }else{
                        Log.d(WiFiDirectActivity.TAG, "NOT ME FORWARDING TO NEW NODE");
                        send(new Socket(InetAddress.getByName(clientIP), port), new FileInputStream(f));
                    }
                }
            }catch(IOException e){
                Log.d(WiFiDirectActivity.TAG, "FAILED TO CONNECT SERVER REASON: " + e.getMessage()); 
            }
        }
        Log.d(WiFiDirectActivity.TAG, "Closing server ");
        try{
            server.close();
            server = null;
        }catch(Exception e){
            Log.d(WiFiDirectActivity.TAG, "FAILED TO CLOSE SERVER");
        }
    }
    
    public static Socket connect(String Address, int port){
        Socket socket = null;
        try{
            Log.d(WiFiDirectActivity.TAG, "TRYING TO CONNECT TO " + Address);
            socket = new Socket(InetAddress.getByName(Address), port);
        }catch(IOException e){
            Log.d(WiFiDirectActivity.TAG, "FAILED TO CREATE SOCKET REASON: " + e.getMessage());
            return null;
        }
        return socket;
    }
}
