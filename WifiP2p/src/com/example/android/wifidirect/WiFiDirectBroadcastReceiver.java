/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.wifidirect;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * A BroadcastReceiver that notifies of important wifi p2p events.
 */
public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

    final private WifiP2pManager manager;
    final private Channel channel;
    private WifiP2pGroup connected;
    private WiFiDirectActivity activity;

    /**
     * @param manager WifiP2pManager system service
     * @param channel Wifi p2p channel
     * @param activity activity associated with the receiver
     */
    public WiFiDirectBroadcastReceiver(WifiP2pManager manager, Channel channel,
            WiFiDirectActivity activity) {
        super();
        this.manager = manager;
        this.channel = channel;
        this.activity = activity;
    }

    /*
     * (non-Javadoc)
     * @see android.content.BroadcastReceiver#onReceive(android.content.Context,
     * android.content.Intent)
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(WiFiDirectActivity.TAG, "Intent is " + action + " CONTEXT IS " + context);
        final Intent beacon = new Intent();
        //beacon.setAction(manager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        //beacon.setAction(manager.WIFI_P2P_PEERS_CHANGED_ACTION);
        //beacon.setAction(manager.WIFI_P2P_STATE_CHANGED_ACTION);
        beacon.setAction("android.intent.action.ALIVE");
        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {

            // UI update to indicate wifi p2p status.
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                // Wifi Direct mode is enabled
                activity.setIsWifiP2pEnabled(true);
            } else {
                activity.setIsWifiP2pEnabled(false);
                activity.resetData();
            }
            Log.d(WiFiDirectActivity.TAG, "P2P state changed - " + state);
        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {

            // request available peers from the wifi p2p manager. This is an
            // asynchronous call and the calling activity is notified with a
            // callback on PeerListListener.onPeersAvailable()
            if (manager != null) {
                manager.requestPeers(channel, (PeerListListener) activity.getFragmentManager()
                        .findFragmentById(R.id.frag_list));
            }
            //Log.d(WiFiDirectActivity.TAG, "P2P peers changed");
            //Need to distribute the peerlist that is held by the GOO
            
            if(connected!= null){
                DeviceListFragment List = (DeviceListFragment) activity.getFragmentManager().findFragmentById(R.id.frag_list);
                //List.setPeers(connected.getClientList());
                if(connected.isGroupOwner()){
                    List.setPeers(connected);
                }
                Log.d(WiFiDirectActivity.TAG, "P2P peers changed\n Now Printing list: "+ connected.getClientList().toString());
            }else{
                Log.d(WiFiDirectActivity.TAG, "NO GROUP");
            }
        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            if (manager == null) {
                return;
            }
            Thread t = null;
            NetworkInfo networkInfo = (NetworkInfo) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
            if (networkInfo.isConnected()) {
                // we are connected with the other device, request connection
                // info to find group owner IP
                
                //create a server here
                t = new Thread(new Runnable(){
                    public void run(){
                        try{
                            Sox.startServer();
                            Log.d(WiFiDirectActivity.TAG, "starting server");
              //              Sox.server(55555);
                            Sox.UDPserver();
                        }catch(Exception e){
                            e.printStackTrace();
                            Log.d(WiFiDirectActivity.TAG, "FAILED TO START A SERVER " + e.getMessage());
                        }
                    }
                });
                try{
                    t.start();
                }catch(Exception e){
                    Log.d(WiFiDirectActivity.TAG, "COULDN'T START SERVER");
                }
                //
                Log.d(WiFiDirectActivity.TAG, "NetWork information: " + networkInfo.toString());
                //added by me
                final DeviceListFragment List = (DeviceListFragment) activity.getFragmentManager().findFragmentById(R.id.frag_list);
                final DeviceDetailFragment fragment = (DeviceDetailFragment) activity
                        .getFragmentManager().findFragmentById(R.id.frag_detail);
                manager.requestConnectionInfo(channel, fragment);
                manager.requestGroupInfo(channel, new WifiP2pManager.GroupInfoListener(){
                    @Override
                    public void onGroupInfoAvailable(final WifiP2pGroup group){
                        if(group != null){
                            connected= group;
                            if(group.isGroupOwner()){
                                List.setPeers(group);

                            }
                            Log.d(WiFiDirectActivity.TAG, "Initial P2P peers \n Now Printing list: "+ group.getClientList().toString());
                        }else{
                            Log.d(WiFiDirectActivity.TAG, "GROUP IS DNE"); 
                        }
                    }
                });
            } else {
                try{
                    t.join();
                    t = null;
                }catch(Exception e){
                    Log.d(WiFiDirectActivity.TAG, "COULDN'T JOIN THREADS");
                }
                // It's a disconnect
                Log.d(WiFiDirectActivity.TAG, "NO LONGER CONNECTED");
                activity.resetData();
                activity.Discover();
                connected = null;
            }
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            DeviceListFragment fragment = (DeviceListFragment) activity.getFragmentManager()
                    .findFragmentById(R.id.frag_list);
            fragment.updateThisDevice((WifiP2pDevice) intent.getParcelableExtra(
                    WifiP2pManager.EXTRA_WIFI_P2P_DEVICE));
        }else if (action.equals("ALIVE")){
           Log.d(WiFiDirectActivity.TAG, "RECEIVED BROADCAST"); 
        }
    }
}
