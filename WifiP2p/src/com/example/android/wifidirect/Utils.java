package com.example.android.wifidirect;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.lang.Integer;
import java.util.Scanner;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import android.util.Log;
import java.util.ArrayList;
import java.util.Hashtable;

public class Utils {
    private static int AddBook = 0;
    public static String[] address = null;
    public static Hashtable<String, String> Route = new Hashtable<String, String>();
    public static ArrayList<String> YellowPages = new ArrayList<String>();

	private final static String p2pInt = "p2p-p2p0";
    public static String[] getARP(){
        String[] arp = null;
        BufferedReader bf = null;
        int i = 0;
        try{
           bf = new BufferedReader(new FileReader("/proc/net/arp"));
           String line = null;
	       while ((line = bf.readLine()) != null) {
               arp[i++] = line;
           }
        }catch(Exception e){
            Log.d(WiFiDirectActivity.TAG, "Error reading in the arp table " + e.getMessage());
            return null;
        }finally {
            try{
                bf.close();
            }catch(Exception e){
                Log.d(WiFiDirectActivity.TAG, "FAILED TO CLOSE READER " + e.getMessage());
                return null;
            }
        }
        return arp; 
    }
    /*
    public static String getIPFromMac(String MAC) {
        String result = null;
		BufferedReader br = null;
        Scanner sk = null;
		try {
			br = new BufferedReader(new FileReader("/proc/net/arp"));
			String line;
			while ((line = br.readLine()) != null) {
           // while((line = sk.nextLine()) != null){
                //line = sk.nextLine();
                Log.d(WiFiDirectActivity.TAG, "ARP IS REPORTING \n" + line);
				String[] splitted = line.split(" +");
				if (splitted != null && splitted.length >= 4) {
					// Basic sanity check
                    for(int i = 0; i < splitted.length; i++){
                        Log.d(WiFiDirectActivity.TAG, "i is " + i + " MAC IS|||" + splitted[i] + "|||mac is|||" + MAC + "|||");
                                
                        String device = splitted[5];
                        if (device.matches(".*" +p2pInt+ ".*")){
                            String mac = converter(splitted[3]);
                            Log.d(WiFiDirectActivity.TAG, "fixed mac is " + mac);
                            if (mac.matches(MAC)) {//used to be matches not equals
                                result = splitted[0];
                                Log.d(WiFiDirectActivity.TAG, "Splitted IS " + splitted[0]);
                            }else{
                                Log.d(WiFiDirectActivity.TAG, "COULDING FIND MAC");
                            }
                        }else{
                            Log.d(WiFiDirectActivity.TAG, "COULDN'T SPLIT PROPERLY");
                        }
                    }

				}else{
                    Log.d(WiFiDirectActivity.TAG, "SPLITTED IS NULL");
                }
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//return null;
        Log.d(WiFiDirectActivity.TAG, "ADDRESS IS " + result);
        return result;
	}
    */
	public static String getIPFromMac(String MAC) {
        updateTable();
        return Route.get(MAC);
    }
    public static String[] getAddress(){
        updateTable();
        YellowPages = new ArrayList<String>(Route.values());    
        String[] neighbors = new String[YellowPages.size()];
        Log.d(WiFiDirectActivity.TAG, "DIRECTORY IS " + YellowPages.size()); 
        try{
            YellowPages.toArray(neighbors);
        }catch(ArrayStoreException e){
            e.printStackTrace();
        }catch(NullPointerException e){
            e.printStackTrace();
        }
        return neighbors;
    }
    public static void updateTable(){
        String result = null;
		BufferedReader br = null;
        Scanner sk = null;
		try {
			br = new BufferedReader(new FileReader("/proc/net/arp"));
			String line;
			while ((line = br.readLine()) != null) {
           // while((line = sk.nextLine()) != null){
                //line = sk.nextLine();
                //Log.d(WiFiDirectActivity.TAG, "ARP IS REPORTING \n" + line);
				String[] splitted = line.split(" +");
				if (splitted != null && splitted.length >= 4) {
					// Basic sanity check
                    for(int i = 0; i < splitted.length; i++){
                                
                        String device = splitted[5];
                        if (device.matches(".*" +p2pInt+ ".*")){
                            String mac = converter(splitted[3]);
                                result = splitted[0];
                            if(!Route.contains(mac)){
                                //maybe add more checks?
                                Route.put(mac, result);
                            }
                        }else{
          //                  Log.d(WiFiDirectActivity.TAG, "COULDN'T SPLIT PROPERLY");
                        }
                    }

				}else{
                    Log.d(WiFiDirectActivity.TAG, "SPLITTED IS NULL");
                }
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

    public static String converter(String mac){
        //177 -> 49
        //140 -> 12
        //need to shift by 128
        String[] parts = mac.split(":");
        String[] error = parts[4].split("(?<=\\G.{1})");
        String corrected = null;

        if(error[0].equals("0")){
            corrected = "8";
        }else if(error[0].equals("1")){
            corrected = "9";
        }else if(error[0].equals("2")){
            corrected = "a";
        }else if(error[0].equals("3")){
            corrected = "b";
        }else if(error[0].equals("4")){
            corrected = "c";
        }else if(error[0].equals("5")){
            corrected = "d";
        }else if(error[0].equals("6")){
            corrected = "e";
        }else if(error[0].equals("7")){
            corrected = "f";
        }else if(error[0].equals("8")){
            corrected = "0";
        }else if(error[0].equals("9")){
            corrected = "1";
        }else if(error[0].equals("a")){
            corrected = "2";
        }else if(error[0].equals("b")){
            corrected = "3";
        }else if(error[0].equals("c")){
            corrected = "4";
        }else if(error[0].equals("d")){
            corrected = "5";
        }else if(error[0].equals("d")){
            corrected = "6";
        }else if(error[0].equals("f")){
            corrected = "7";
        }else{
            corrected = error[0];
        }
        String fixed = new String(corrected+error[1]);
        return new String(parts[0]+":"+parts[1]+":"+parts[2]+":"+parts[3]+":"+fixed+":"+parts[5]);
    }
	public static String getLocalIPAddress() {
		/*
		 * modified from:
		 * 
		 * http://thinkandroid.wordpress.com/2010/03/27/incorporating-socket-programming-into-your-applications/
		 * 
		 * */
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();

					String iface = intf.getName();
					if(iface.matches(".*" +p2pInt+ ".*")){
						if (inetAddress instanceof Inet4Address) { // fix for Galaxy Nexus. IPv4 is easy to use :-)
							return getDottedDecimalIP(inetAddress.getAddress());
						}
					}
				}
			}
		} catch (SocketException ex) {
			Log.e("AndroidNetworkAddressFactory", "getLocalIPAddress()", ex);
		} catch (NullPointerException ex) {
			Log.e("AndroidNetworkAddressFactory", "getLocalIPAddress()", ex);
		}
		return null;
	}

	private static String getDottedDecimalIP(byte[] ipAddr) {
		/*
		 * ripped from:
		 * 
		 * http://stackoverflow.com/questions/10053385/how-to-get-each-devices-ip-address-in-wifi-direct-scenario
		 * 
		 * */
		String ipAddrStr = "";
		for (int i=0; i<ipAddr.length; i++) {
			if (i > 0) {
				ipAddrStr += ".";
			}
			ipAddrStr += ipAddr[i]&0xFF;
		}
		return ipAddrStr;
	}
}
