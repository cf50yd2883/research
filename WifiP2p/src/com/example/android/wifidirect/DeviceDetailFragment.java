/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.wifidirect;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.wifidirect.DeviceListFragment.DeviceActionListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
//added by me

import java.net.DatagramSocket;
import java.net.DatagramPacket;
import android.media.AudioTrack;
import android.media.AudioFormat;
import android.media.MediaRecorder;
import java.net.InetAddress;
import android.media.MediaPlayer;
import android.media.AudioManager;
import android.view.MotionEvent;
import java.net.InetSocketAddress;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.net.InetSocketAddress;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.Timer;
import java.util.TimerTask;

//for streaming
import android.os.ParcelFileDescriptor;
import java.io.FileDescriptor;

/**
 * A fragment that manages a particular peer and allows interaction with device
 * i.e. setting up network connection and transferring data.
 */
//public class DeviceDetailFragment extends Fragment {
public class DeviceDetailFragment extends Fragment implements ConnectionInfoListener {

    protected static final int CHOOSE_FILE_RESULT_CODE = 20;
    private View mContentView = null;
    private WifiP2pDevice device;
    private WifiP2pInfo info;
    ProgressDialog progressDialog = null;
    public boolean record = false; 
    //temp added
    //private MediaPlayer mPlayer = null;
    private MediaRecorder mRecorder = null;
    private static String mFileName = "/storage/emulated/0/DCIM/Audio";
    //private static String mFileName = "/mnt/sdcard/DCIM/Audio.pcm";
    private String client_mac_fixed;
    boolean running = false;
    boolean GO = false;
    String OwnerAddress;
    private Thread t = null;
    ServerSocket server;
    Socket client;
    public static final int PORT = 55555;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    } 

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
        mContentView = inflater.inflate(R.layout.device_detail, null);
        mContentView.findViewById(R.id.btn_NOA).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //do the NoA with the config
                ((DeviceActionListener) getActivity()).NoA();
            }
        });
        mContentView.findViewById(R.id.btn_NOW).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //add back to prior net
                ((DeviceActionListener) getActivity()).NoW();
            }
        });
        mContentView.findViewById(R.id.btn_connect).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WifiP2pConfig config = new WifiP2pConfig();
                config.deviceAddress = device.deviceAddress;
                config.wps.setup = WpsInfo.PBC;
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                progressDialog = ProgressDialog.show(getActivity(), "Press back to cancel",
                        "Connecting to :" + device.deviceAddress, true, true
//                        new DialogInterface.OnCancelListener() {
//
//                            @Override
//                            public void onCancel(DialogInterface dialog) {
//                                ((DeviceActionListener) getActivity()).cancelDisconnect();
//                            }
//                        }
                        );
                ((DeviceActionListener) getActivity()).connect(config);

            }
        });

        mContentView.findViewById(R.id.btn_disconnect).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        ((DeviceActionListener) getActivity()).disconnect();
                        running = false;
                        t = null;
                        Log.d(WiFiDirectActivity.TAG, "DISCONNECTING");
                    }
                });
         
        mContentView.findViewById(R.id.btn_start_client).setOnTouchListener(
                new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        switch(event.getAction()){
                            case MotionEvent.ACTION_DOWN:
                                //Audio.Talking(mFileName);
                                // for sreamingin 
                                FileDescriptor outputPipe = null;
                                FileDescriptor InputPipe = null;
                                ParcelFileDescriptor[] pipe = null;
                                try{
                                    pipe = ParcelFileDescriptor.createPipe();
                                    outputPipe = pipe[1].getFileDescriptor();
                                }catch(IOException e){
                                    Log.d(WiFiDirectActivity.TAG, "COULDN'T MAKE PIPES " + e.getMessage());
                                }
                                Audio.Talking(pipe, client_mac_fixed, 12345 );
                                StreamData(pipe);
                                record = true;
                                break;
                            case MotionEvent.ACTION_UP:
                                t = null;
                                Audio.stopTalking();
                                record = false;
                                break;
                        }
                        /*
                        if(record != true){
                            sendData();
                        }*/
                        return false;
                        //getActivity().startService(serviceIntent);
                    }
                });
                
                return mContentView;
    }
    public void StreamData(ParcelFileDescriptor[] Pipes){
     final String clientIP;
        if(GO){
            clientIP = Utils.getIPFromMac(client_mac_fixed);
        }else{
            clientIP = OwnerAddress;
        }
        Log.d(WiFiDirectActivity.TAG, "SENDING TO " + clientIP);
        final ParcelFileDescriptor pipe = Pipes[0];

        try{
            t = new Thread(new Runnable(){
                public void run(){
                    int port = 12345;
                    try{
                        //byte[] address = client_mac_fixed.getBytes();
                        Audio.PrepWaveFile(client_mac_fixed, port, pipe);
                    }catch(Exception e){
                        Log.d(WiFiDirectActivity.TAG, "FAILED TO CONNECTE AND SEND ");
                    }
                 }
             }); 
            t.start();
         }catch(Exception e){
             Log.d(WiFiDirectActivity.TAG, "FAILED TO SEND");
         }
    }
    public void sendData(){
        final String clientIP;
        if(GO){
            clientIP = Utils.getIPFromMac(client_mac_fixed);
        }else{
            clientIP = OwnerAddress;
        }
        Log.d(WiFiDirectActivity.TAG, "SENDING TO " + clientIP);
        //final String[] arp = Utils.getARP(); 
        final File inputFile = new File(mFileName+".wav");
        //final FileDescriptor inputFile = Pipes[0].getFileDescriptor();
        try{
            final byte[] stuff = new byte[(int) inputFile.length()];
            FileInputStream fis = new FileInputStream(inputFile);
            int len = fis.read(stuff);
            Thread Piped = new Thread(new Runnable(){
                public void run(){
                    Socket socket = null;
                    try{
                    byte[] address = client_mac_fixed.getBytes();
                    if(client_mac_fixed.equals(Sox.Broad)){
                        String[] Directory = Utils.getAddress();
                        for(int i = 0; i < Directory.length; i++){
                            Log.d(WiFiDirectActivity.TAG, "SENDING TO " + Directory[i]);
                            Log.d(WiFiDirectActivity.TAG, "LENGTH IS " + (int)inputFile.length());
                            Sox.send(new Socket(InetAddress.getByName(Directory[i]), PORT), new FileInputStream(inputFile));
                      //      Sox.UDPsend(Directory[i], 12345, stuff, 4096); 
                        }
                    }else{
                        Log.d(WiFiDirectActivity.TAG, "LOOSE");
                        socket = Sox.connect(clientIP, PORT);
                        Sox.send(socket, new FileInputStream(inputFile));
                       //Sox.UDPsend(clientIP, 12345, stuff, 4096);
                    }
                    }catch(Exception e){
                        Log.d(WiFiDirectActivity.TAG, "FAILED TO CONNECTE AND SEND ");
                    }
                 }
             });
            Piped.start();
            Piped.join();
         }catch(Exception e){
             Log.d(WiFiDirectActivity.TAG, "FAILED TO SEND");
         }
    }
    //Don't need this anymore) 
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User has picked an image. Transfer it to group owner i.e peer using
        // FileTransferService.
        Uri uri = data.getData();
        TextView statusText = (TextView) mContentView.findViewById(R.id.status_text);
        statusText.setText("Sending: " + uri);
        Log.d(WiFiDirectActivity.TAG, "Intent----------- " + uri);
        Intent serviceIntent = new Intent(getActivity(), FileTransferService.class);
        serviceIntent.setAction(FileTransferService.ACTION_SEND_FILE);
        serviceIntent.putExtra(FileTransferService.EXTRAS_FILE_PATH, uri.toString());
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_ADDRESS,
                info.groupOwnerAddress.getHostAddress());
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_PORT, PORT);
        //port was originally 8988
        getActivity().startService(serviceIntent);
    }

    @Override
    public void onConnectionInfoAvailable(final WifiP2pInfo info) {
        Log.d(WiFiDirectActivity.TAG, "Cononection info available at " + System.currentTimeMillis()+ " time ");
        running = true;
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        this.info = info;
        this.getView().setVisibility(View.VISIBLE);

        // The owner IP is now known.
        TextView view = (TextView) mContentView.findViewById(R.id.group_owner);
        view.setText(getResources().getString(R.string.group_owner_text)
                + ((info.isGroupOwner == true) ? getResources().getString(R.string.yes)
                        : getResources().getString(R.string.no)));
        // InetAddress from WifiP2pInfo struct.
        
        view = (TextView) mContentView.findViewById(R.id.device_info);
        final int port = 55555;
        if(!info.isGroupOwner){
            GO = false;
            OwnerAddress = info.groupOwnerAddress.getHostAddress();
        }else{
            GO = true;
       }
    
        // After the group negotiation, we assign the group owner as the file
        // server. The file server is single threaded, single connection server
        // socket.
      /* 
        new Thread(new Runnable(){
            public void run(){
                try{
                    Sox.startServer();
                    Sox.server(PORT);
                }catch(Exception e){
                    Log.d(WiFiDirectActivity.TAG, "FAILED TO CREATE SERVER " + e.getMessage());
                }
            }
        }).start();
        */
        // The other device acts as the client. In this case, we enable the
        // get file button.
        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.VISIBLE);
        ((TextView) mContentView.findViewById(R.id.status_text)).setText(getResources()
                .getString(R.string.client_text));

        // hide the connect button
        mContentView.findViewById(R.id.btn_connect).setVisibility(View.GONE);
    }

    /**
     * Updates the UI with device data
     * 
     * @param device the device to be displayed
     */
    public void showDetails(WifiP2pDevice device) {
        this.device = device;
        client_mac_fixed = device.deviceAddress;
        Sox.setMac(client_mac_fixed);
        this.getView().setVisibility(View.VISIBLE);
        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
        String ipaddress = Utils.getIPFromMac(client_mac_fixed);
        view.setText("Client IP is " + ipaddress);
        //view.setText(device.deviceAddress);
        view.setText(client_mac_fixed);
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText(device.toString());

    }

    /**
     * Clears the UI fields after a disconnect or direct mode disable operation.
     */
    public void resetViews() {
        mContentView.findViewById(R.id.btn_connect).setVisibility(View.VISIBLE);
        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.group_owner);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.status_text);
        view.setText(R.string.empty);
        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
        this.getView().setVisibility(View.GONE);
    }

    /**
     * A simple server socket that accepts connection and writes some data on
     * the stream.
     */
}
