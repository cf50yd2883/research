# view land/res/layout-land/main.xml #generated:19
# view large/res/layout-large/main.xml #generated:12
# view res/layout/main.xml #generated:19
-keep class com.example.android.wifidirect.DeviceDetailFragment { <init>(...); }

# view land/res/layout-land/main.xml #generated:12
# view large/res/layout-large/main.xml #generated:7
# view res/layout/main.xml #generated:12
-keep class com.example.android.wifidirect.DeviceListFragment { <init>(...); }

# view AndroidManifest.xml #generated:35
-keep class com.example.android.wifidirect.FileTransferService { <init>(...); }

# view AndroidManifest.xml #generated:23
-keep class com.example.android.wifidirect.WiFiDirectActivity { <init>(...); }

