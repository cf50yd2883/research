package com.example.android.audioapp;

import android.app.Activity;
import android.widget.LinearLayout;
import android.os.Environment;
import android.view.ViewGroup;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Context;
import android.util.Log;
import android.media.MediaRecorder;
import android.media.MediaPlayer;
import android.os.Bundle;
//for me
import android.media.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileOutputStream;
import java.lang.Thread;
public class AudioActivity extends Activity
{
    //mine
    int BufferElements2Rec = 1024;
    int BytesPerElement = 2;
    private boolean isRecording = false;
    private Thread recordingThread = null;
    private AudioRecord recorder = null;
    private static final int RECORDER_SAMPLERATE = 8000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    //google's
    private static final String LOG_TAG = "AudioRecordTeset";
    private static String mFileName = "/mnt/sdcard/DCIM/AudioTest.pcm";
    private RecordButton mRecordButton = null;
    private MediaRecorder mRecorder = null;
    private PlayButton mPlayButton = null;
    private MediaPlayer mPlayer = null;
/*
    public AudioRecordTest(){
        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/audiorecordtest.3gp";
    }*/
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle)
    {
        super.onCreate(icicle);
        LinearLayout ll = new LinearLayout(this);
        mRecordButton = new RecordButton(this);
        ll.addView(mRecordButton,
                new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    0));
        mPlayButton = new PlayButton(this);
        ll.addView(mPlayButton, 
                new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    0));
        setContentView(ll);
    }
    @Override
    public void onPause(){
        super.onPause();
        if(mRecorder != null){
            mRecorder.release();
            mRecorder = null;
        }
        if(mPlayer != null){
            mPlayer.release();
            mPlayer = null;
        }
    }
    class RecordButton extends Button{
        boolean mStartRecording = true;
        
        OnClickListener clicker = new OnClickListener(){
            public void onClick(View v){
                onRecord(mStartRecording);
                if(mStartRecording){
                    setText("Stop recording");
                }else{
                    setText("start recording");
                }
                mStartRecording = !mStartRecording;
            
            }
        };

        public RecordButton(Context ctx){
            super(ctx);
            setText("Start Recording");
            setOnClickListener(clicker);
        }
    }
    class PlayButton extends Button{
        boolean mStartPlaying = true;
        OnClickListener clicker = new OnClickListener(){
            public void onClick(View v){
                onPlay(mStartPlaying);
                if(mStartPlaying){
                    setText("Stop Playing");
                }else{
                    setText("Start Playing");
                }
                mStartPlaying = !mStartPlaying;
            }
        };
        public PlayButton(Context ctx){
            super(ctx);
            setText("Start Playing");
            setOnClickListener(clicker);
        }
    }
    
    //google method of audio capture
    private void onRecord(boolean start){
        if(start){
            start_Recording();
            //startRecording();
        }else{
            stop_Recording();
            //stopRecording();
        }
    }
    private void start_Recording(){
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try{
            mRecorder.prepare();
        }catch(IOException e){
            Log.e(LOG_TAG, "prepare() failed");
        }
        mRecorder.start();
    }
    private void stop_Recording(){
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
    }
    //google audio playbackk
    private void onPlay(boolean start){
        if(start){
            startPlaying();
        }else{
            stopPlaying();
        }
    }
    private void startPlaying(){
        mPlayer = new MediaPlayer();
        try{
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        }catch(IOException e){
            Log.e(LOG_TAG, "prepare() failed");
        }
    }
    private void stopPlaying(){
        mPlayer.release();
        mPlayer = null;
    }
    //my method of audio capture
    
    public void startRecording(){
        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, BufferElements2Rec*BytesPerElement);
        recorder.startRecording();
        isRecording = true;
        recordingThread = new Thread(new Runnable(){public void run(){writeAudioDataToFile();}}, "AudioRecorder Thread");
        recordingThread.start();
    }
    private byte[] short2Byte(short[] sData){
        int shortArrSize = sData.length;
        byte[] bytes = new byte[shortArrSize*2];
        for(int i = 0; i < shortArrSize; i++){
            bytes[i*2] = (byte)(sData[i] & 0x00FF);
            bytes[(i*2)+1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }   
        return bytes;
    }
    private void writeAudioDataToFile(){
        short sData[] = new short[BufferElements2Rec];
        FileOutputStream os = null;
        try{
            os = new FileOutputStream(mFileName);
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        while(isRecording){
            recorder.read(sData, 0, BufferElements2Rec);
            try{
                byte bData[] = short2Byte(sData);
                os.write(bData, 0, BufferElements2Rec*BytesPerElement);
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        try{
            os.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    private void stopRecording(){
        if(null != recorder){
            isRecording = false;
            recorder.stop();
            recorder.release();
            recorder = null;
            recordingThread = null;
        }
    }
}
