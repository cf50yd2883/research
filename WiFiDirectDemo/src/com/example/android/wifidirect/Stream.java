package com.example.android.wifidirect;
import android.os.Bundle;
import android.os.Environment;
import android.content.Intent;
import android.content.ContentResolver;
import android.content.Context;
import android.app.IntentService;
import android.util.Log;
import android.media.AudioTrack;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.DatagramSocket;
import java.lang.Thread;


public class Stream extends IntentService{
    private static final int SOCKET_TIMEOUT = 5000;
    public static final String ACTION_SPEAK = "com.example.android.wifidirect.SPEAK";
    public static final String EXTRAS_GROUP_OWNER_PORT =  "go_port";//should be 50005;
    public static final String EXTRAS_GROUP_OWNER_ADDRESS = "go_host";
    //need the IP address - how does it get it?
    //port of GO
    private boolean currentlySendingAudio = false;
    //	Does the sample rate need to be so high for voice recordings?
    //	Is there a way to control the sampling rate from the GUI or at some level w/in the program? 
    private static final int RECORDING_RATE = 44100;
    private static final int CHANNEL = AudioFormat.CHANNEL_IN_MONO;
    private static final int FORMAT = AudioFormat.ENCODING_PCM_16BIT;
    //private static final int PORT = 50005;
    //add the button here?
    private AudioRecord recorder;
    private static int BUFFER_SIZE = AudioRecord.getMinBufferSize(RECORDING_RATE, CHANNEL, FORMAT);
    //not sure what this does
    public Stream(String name){
        super(name);
    }
    public Stream(){
        super("SPEAK");
    }
    /*
    //do i add the booleans for streaming here?
    private void startStreamingAudio(){
        //Log.i(TAG, "Starting the Audio Stream");
        currentlySendingAudio = true;
        startStreaming();
    }
    private void stopStreamingAudio(){
        //Log.i(TAG, "Stoping the Audio Stream");
        currentlySendingAudio = false;
        recorder.release();
    }
    */
    @Override
    protected void onHandleIntent(Intent intent){
        Context context = getApplicationContext();
        if(intent.getAction().equals(ACTION_SPEAK)){
            currentlySendingAudio = true;
            int PORT = intent.getExtras().getInt(EXTRAS_GROUP_OWNER_PORT);
            String GO = intent.getExtras().getString(EXTRAS_GROUP_OWNER_ADDRESS);
            startStreaming(GO, PORT);
        }else{
            currentlySendingAudio = false;
            recorder.release();
        }
    }
    private void startStreaming(final String GO, final int PORT){
        //Log.i(TAG, "Starting the stream to socket");
        Thread streamThread = new Thread(new Runnable(){
            @Override
            public void run(){
                try{
                    //might change to tcp socket?
                    DatagramSocket socket = new DatagramSocket();
                    //Socket socket = new Socket();

                    byte[] buffer = new byte[BUFFER_SIZE];
                    
                    final InetAddress LOCATION = InetAddress.getByName(GO);//need to change to something real

                    DatagramPacket packet;//make my own packet class?
                    //prep and commands to use the mic
                    recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDING_RATE, CHANNEL, FORMAT, BUFFER_SIZE*10);
                    recorder.startRecording();
                    while(currentlySendingAudio == true){
                        int read = recorder.read(buffer, 0, buffer.length);
                        //might not be needed
                        packet = new DatagramPacket(buffer, read, LOCATION, PORT);
                        socket.send(packet);
                    }
                }catch(Exception e){
                    //Log.e(TAG, "Exception: " + e);
                }
            }
        });
        streamThread.start();
    }
}
