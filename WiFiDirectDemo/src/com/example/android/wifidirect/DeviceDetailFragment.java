/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.wifidirect;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.wifidirect.DeviceListFragment.DeviceActionListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
//added by me

import java.net.DatagramSocket;
import java.net.DatagramPacket;
import android.media.AudioTrack;
import android.media.AudioFormat;
import android.media.MediaRecorder;
import java.net.InetAddress;
import android.media.MediaPlayer;
import android.media.AudioManager;
import android.view.MotionEvent;
import java.net.InetSocketAddress;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.net.InetSocketAddress;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

/**
 * A fragment that manages a particular peer and allows interaction with device
 * i.e. setting up network connection and transferring data.
 */
public class DeviceDetailFragment extends Fragment implements ConnectionInfoListener {

    protected static final int CHOOSE_FILE_RESULT_CODE = 20;
    private View mContentView = null;
    private WifiP2pDevice device;
    private WifiP2pInfo info;
    ProgressDialog progressDialog = null;
    public boolean record = false; 
    //temp added
    //private MediaPlayer mPlayer = null;
    private MediaRecorder mRecorder = null;
    private static String mFileName = "/mnt/sdcard/DCIM/Audio.pcm";
    private String client_mac_fixed;
    boolean running = false;
    private Thread t = null;
    ServerSocket server;
    Socket client;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
        mContentView = inflater.inflate(R.layout.device_detail, null);
        mContentView.findViewById(R.id.btn_connect).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WifiP2pConfig config = new WifiP2pConfig();
                config.deviceAddress = device.deviceAddress;
                config.wps.setup = WpsInfo.PBC;
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                progressDialog = ProgressDialog.show(getActivity(), "Press back to cancel",
                        "Connecting to :" + device.deviceAddress, true, true
//                        new DialogInterface.OnCancelListener() {
//
//                            @Override
//                            public void onCancel(DialogInterface dialog) {
//                                ((DeviceActionListener) getActivity()).cancelDisconnect();
//                            }
//                        }
                        );
                ((DeviceActionListener) getActivity()).connect(config);

            }
        });

        mContentView.findViewById(R.id.btn_disconnect).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        ((DeviceActionListener) getActivity()).disconnect();
                        running = false;
                        t = null;
                        Log.d(WiFiDirectActivity.TAG, "DISCONNECTING");
                    }
                });
         
        mContentView.findViewById(R.id.btn_start_client).setOnTouchListener(
                new View.OnTouchListener() {
//need to change here to record the audio.
                    public boolean onTouch(View v, MotionEvent event) {
                        //Intent serviceIntent = new Intent(getActivity(), VoiceRecordingService.class);
                        switch(event.getAction()){
                            case MotionEvent.ACTION_DOWN:
                                //start an intent of our media recorder
                                //serviceIntent.setAction(VoiceRecordingService.ACTION_RECORD_START);

                                startRecording(); 
                                record = true;
                                break;
                            case MotionEvent.ACTION_UP:
                                //serviceIntent.setAction(VoiceRecordingService.ACTION_STOP_RECORDING);

                                stopRecording();
                                record = false;
                                break;
                        }
                        if(record != true){
                            sendData();
                        }
                        return false;
                        //getActivity().startService(serviceIntent);
                    }
                });
                
                return mContentView;
    }

    public void startRecording(){
TextView statusText = (TextView) mContentView.findViewById(R.id.status_text);
                statusText.setText("Starting: ");


        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try{
            mRecorder.prepare();
        }catch(IOException e){
        }
        mRecorder.start();

    }
    public void stopRecording(){
                TextView statusText = (TextView) mContentView.findViewById(R.id.status_text);
                statusText.setText("Stopping: ");

        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        Log.d(WiFiDirectActivity.TAG, "Saved Audio Track");
    }
    
    public void sendData(){
        final String clientIP = Utils.getIPFromMac(client_mac_fixed);
        final File file = new File(mFileName);
        final TextView statusText = (TextView) mContentView.findViewById(R.id.status_text);
                statusText.setText("Try Sending: to  " + info.groupOwnerAddress.getHostAddress());
        Thread d = new Thread(new Runnable(){
            public void run(){
                try{
                    //Socket socket = new Socket(info.groupOwnerAddress.getHostAddress(), 55555);          
                    Socket socket = new Socket(InetAddress.getByName(clientIP), 55555);
                    if(socket.isConnected()){
                        statusText.post(new Runnable(){
                            public void run(){
                                statusText.setText("Connected to Server");
                            }
                        });
                        OutputStream out = socket.getOutputStream();
                        FileInputStream inputStream = new FileInputStream(file);
                        byte buf[] = new byte[1024];
                        int len;
                        try {
                            while ((len = inputStream.read(buf)) != -1) {
                                out.write(buf, 0, len);
                                out.flush();
                                Log.d(WiFiDirectActivity.TAG, "WRITING FILE");
                            }
                            out.close();
                            inputStream.close();
                        } catch (IOException e) {
                            Log.d(WiFiDirectActivity.TAG, e.toString());
                        }
                        socket.close();
                    }
                }catch(Exception e){
                        statusText.post(new Runnable(){
                            public void run(){
                                statusText.setText("Failed to create Server");
                            }
                        });
                }
            }
        });
        d.start();
        try{
            d.join();
        }catch(InterruptedException e){

            statusText.post(new Runnable(){
                public void run(){
                    statusText.setText("thread failed to join");
                }
            });
        }
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // User has picked an image. Transfer it to group owner i.e peer using
        // FileTransferService.
        Uri uri = data.getData();
        TextView statusText = (TextView) mContentView.findViewById(R.id.status_text);
        statusText.setText("Sending: " + uri);
        Log.d(WiFiDirectActivity.TAG, "Intent----------- " + uri);
        Intent serviceIntent = new Intent(getActivity(), FileTransferService.class);
        serviceIntent.setAction(FileTransferService.ACTION_SEND_FILE);
        serviceIntent.putExtra(FileTransferService.EXTRAS_FILE_PATH, uri.toString());
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_ADDRESS,
                info.groupOwnerAddress.getHostAddress());
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_PORT, 55555);
        //port was originally 8988
        getActivity().startService(serviceIntent);
    }

    @Override
    public void onConnectionInfoAvailable(final WifiP2pInfo info) {
        running = true;
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        this.info = info;
        this.getView().setVisibility(View.VISIBLE);

        // The owner IP is now known.
        TextView view = (TextView) mContentView.findViewById(R.id.group_owner);
        view.setText(getResources().getString(R.string.group_owner_text)
                + ((info.isGroupOwner == true) ? getResources().getString(R.string.yes)
                        : getResources().getString(R.string.no)));

        // InetAddress from WifiP2pInfo struct.
        /*
        view = (TextView) mContentView.findViewById(R.id.device_info);
        if(!info.isGroupOwner){
            view.setText("Group Owner IP - " + info.groupOwnerAddress.getHostAddress());
        }else{
            //view.setText("Group Member IP - " + Utils.getIPFromMac(new String(device.deviceAddress).replace("99", "19")));
           // view.setText("Group Member IP - " + Utils.getIPFromMac(client_mac_fixed));
           String ipaddress = Utils.getIPFromMac(client_mac_fixed);

        }
        */
        // After the group negotiation, we assign the group owner as the file
        // server. The file server is single threaded, single connection server
        // socket.
        
        t = new Thread(new Runnable(){
            //ServerSocket server = null;
            boolean status = true;
            int sampleRate = 11025;
            int bufferSize = 9728;
            int PORT = 8988;
            private MediaPlayer mPlayer = null;
            private String mFile = "/mnt/sdcard/DCIM/rec.pcm";
            File f;
            public void run(){
                try{
                    server = new ServerSocket(55555);
                }catch(Exception e){
                    Log.d(WiFiDirectActivity.TAG, "Failed to create SERVER" + e.getMessage());
                }
                client = null;
                while(running){
                    Log.d(WiFiDirectActivity.TAG, "RUNNING STATUS IS " + Boolean.toString(running));
                    if(!running){
                        try{
                            client.close();
                            server.close();
                        }catch(IOException e){
                            Log.d(WiFiDirectActivity.TAG, "FAiled to close SOckets");
                        }
                    }
                    try{
                        client = server.accept();
                        f= new File(mFile);
                        Log.d(WiFiDirectActivity.TAG, "server: copying files " + f.toString());
                        InputStream inputstream = client.getInputStream();
                        copyFile(inputstream, new FileOutputStream(f));

                    }catch(IOException e){
                        Log.d(WiFiDirectActivity.TAG, "FAILED TO CONNECT SERVER");
                    }
                    startPlay(f);
                }
            }
            public boolean copyFile(InputStream inputStream, OutputStream out) {
                byte buf[] = new byte[1024];
                int len;
                try {
                    while ((len = inputStream.read(buf)) != -1) {
                        out.write(buf, 0, len);
                        out.flush();
                        Log.d(WiFiDirectActivity.TAG, "READING FILE");
                    }
                    out.close();
                    inputStream.close();
                } catch (IOException e) {
                    Log.d(WiFiDirectActivity.TAG, e.toString());
                    return false;
                }
                return true;
            }
            public void startPlay(File file){
                mPlayer = new MediaPlayer();
                while(!file.exists()){
                    int i = 0;
                    i+=1;;
                }
                try{
                    //mPlayer.setDataSource(mFile);
                    //mPlayer.setDataSource(f.toString());
                    String path = file.toString();
                    mPlayer.setDataSource(path);
                    mPlayer.prepare();
                    mPlayer.start();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }

            public void stopPlay(){
                mPlayer.release();
                mPlayer = null;
            }

        });
        t.start();
        // The other device acts as the client. In this case, we enable the
        // get file button.
        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.VISIBLE);
        ((TextView) mContentView.findViewById(R.id.status_text)).setText(getResources()
                .getString(R.string.client_text));

        // hide the connect button
        mContentView.findViewById(R.id.btn_connect).setVisibility(View.GONE);
    }

    /**
     * Updates the UI with device data
     * 
     * @param device the device to be displayed
     */
    public void showDetails(WifiP2pDevice device) {
        this.device = device;
        client_mac_fixed = device.deviceAddress;
        this.getView().setVisibility(View.VISIBLE);
        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
        String ipaddress = Utils.getIPFromMac(client_mac_fixed);
        view.setText("Client IP is " + ipaddress);
        //view.setText(device.deviceAddress);
        view.setText(client_mac_fixed);
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText(device.toString());

    }

    /**
     * Clears the UI fields after a disconnect or direct mode disable operation.
     */
    public void resetViews() {
        mContentView.findViewById(R.id.btn_connect).setVisibility(View.VISIBLE);
        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.group_owner);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.status_text);
        view.setText(R.string.empty);
        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
        this.getView().setVisibility(View.GONE);
    }

    /**
     * A simple server socket that accepts connection and writes some data on
     * the stream.
     */
}


